> Colorado
**Aurora Masters** (Aurora, CO) *Hosted*
https://discord.gg/vm6pBW5

**FRONT RANGE POKEMON COMMUNITY** (Boulder County, CO) *Hosted*
https://discord.gg/gZmhHh3

**Pokemon GO Pueblo Colorado** (Pueblo, CO) *Hosted*
https://discord.gg/wytB9dt

**Pokémon GO Colorado** (Denver, Loveland, Fort Collins, CO at large) *Hosted*
https://discord.gg/6CQzVY5

> Florida
**FGCU PoGo** (Florida Gulf Coast University, FL) *Hosted*
https://discord.gg/grYdEBT

**FloGo Raiders** (Palm Beach/St. Lucie Counties, FL) *Hosted*
https://discord.gg/eDrPfD2

**Naples Raiderz** (Naples, FL) *Hosted*
https://discord.gg/bV79D6r

**PoGo SWFL** (Lee County, FL) *Hosted*
https://discord.gg/yppfXdX

**Pokemon go citrus county** (Citrus County, FL) *Hosted*
https://discord.gg/wNEZshF

**Tampa Bay Area PoGo Hub** (Tampa, FL) *Hosted*
https://discord.gg/daevcFy

> Florida (cont)
**Team Harmony** (Broward County, FL) *Hosted*
https://discord.gg/Sp6KwCp

> Illinois
**ARL HTS** (Arlington Heights, IL) *Self-Hosted*
https://discord.gg/DTbYgE6

**Mount Prospect Pokémon Go** (Mount Prospect, IL) *Hosted*
https://discord.gg/DFzj8e3

**PNR** (Chicago, IL) *Hosted*
https://discord.gg/6fBUHV7

**POGO ISU** (Normal, IL) *Hosted*
https://discord.gg/uqWcyHs

> Michigan
**Sylvan Lake/Keego Harbor** (Sylvan Lake/Keego Harbor, MI) *Hosted*
https://discord.gg/R3hVwjv

> New York
**Pokemon GO: Nassau County** (Nassau County, NY) *Hosted*
https://discord.gg/EmwHv9H

**Pokemon GO: Suffolk County** (Suffolk County, NY) *Hosted*
https://discord.gg/Wg4YZpF

**Pokémon GO North Brooklyn** (Brooklyn, NY) *Hosted*
https://discord.gg/44pqnXG

> Mississippi
Hattiesburg PokemonGo Communty (Hattiesburg, MS) *Hosted*
https://discord.gg/xydaK9v

> Nebraska
Omaha Pogo (Omaha, NE) *Hosted*
https://discord.gg/RH8UHCw

> Pennsylvania
**Beaver County Pokemon Go** (Beaver County, PA) *Self-Hosted*
https://discord.gg/bxacFEb

**Butler Pokemon Go** (Butler County, PA) *Hosted*
https://discord.gg/2qj8NCJ

**Shadow Alliance Raids** (Mercer County, PA) *Self-Hosted*
https://discord.gg/RAUSwhw

> Virginia
**PoGO RVA** (Richmond, VA) *Hosted*
https://discord.gg/7WZAdrp

**Pokémon GO Hampton Roads** (Hampton Roads, VA) *Hosted*
https://discord.gg/kzxt4VW

> Washington
**CapitolHillPokémonGo** (Seattle, WA) *Hosted*
https://discord.gg/xQxns5J